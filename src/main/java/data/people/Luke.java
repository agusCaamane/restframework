package data.people;
import java.util.ArrayList;

public class Luke {

    // Variables
    private static ArrayList vehicles = new ArrayList();
    private static ArrayList films = new ArrayList();
    private static ArrayList starShips = new ArrayList();


    // Values
    private static ArrayList getVehicles() {
        vehicles.add("https://swapi.dev/api/vehicles/14/");
        vehicles.add("https://swapi.dev/api/vehicles/30/");
        return vehicles;
    }

    private static ArrayList getFilms() {
        films.add("https://swapi.dev/api/films/1/");
        films.add("https://swapi.dev/api/films/2/");
        films.add("https://swapi.dev/api/films/3/");
        films.add("https://swapi.dev/api/films/6/");
        return films;
    }

    private static ArrayList getStarShips() {
        starShips.add("https://swapi.dev/api/starships/12/");
        starShips.add("https://swapi.dev/api/starships/22/");
        return starShips;
    }

    // Getters

    public static ArrayList vehicles() {
        vehicles.clear();
        vehicles = getVehicles();
        return vehicles;
    }

    public static ArrayList films() {
        films.clear();
        films = getFilms();
        return films;
    }

    public static ArrayList starShips() {
        starShips.clear();
        starShips = getStarShips();
        return starShips;
    }


}
