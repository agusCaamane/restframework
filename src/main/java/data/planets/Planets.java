package data.planets;
import java.util.ArrayList;

public class Planets {

    // Variables
    private static ArrayList planetsNamesPageOne = new ArrayList();
    private static ArrayList planetsNamesPageTwo = new ArrayList();


    // Values

    public static ArrayList getPlanetsNamesPageOne() {
        planetsNamesPageOne.add("Tatooine");
        planetsNamesPageOne.add("Alderaan");
        planetsNamesPageOne.add("Yavin IV");
        planetsNamesPageOne.add("Hoth");
        planetsNamesPageOne.add("Dagobah");
        planetsNamesPageOne.add("Bespin");
        planetsNamesPageOne.add("Endor");
        planetsNamesPageOne.add("Naboo");
        planetsNamesPageOne.add("Coruscant");
        planetsNamesPageOne.add("Kamino");
        return planetsNamesPageOne;
    }
    public static ArrayList getPlanetsNamesPageTwo() {
        planetsNamesPageTwo.add("Geonosis");
        planetsNamesPageTwo.add("Utapau");
        planetsNamesPageTwo.add("Mustafar");
        planetsNamesPageTwo.add("Kashyyyk");
        planetsNamesPageTwo.add("Polis Massa");
        planetsNamesPageTwo.add("Mygeeto");
        planetsNamesPageTwo.add("Felucia");
        planetsNamesPageTwo.add("Cato Neimoidia");
        planetsNamesPageTwo.add("Saleucami");
        planetsNamesPageTwo.add("Stewjon");
        return planetsNamesPageTwo;
    }


    // Getters

    public static ArrayList planetsNamesPageOne() {
        planetsNamesPageOne.clear();
        planetsNamesPageOne = getPlanetsNamesPageOne();
        return planetsNamesPageOne;
    }

    public static ArrayList planetsNamesPageTwo() {
        planetsNamesPageTwo.clear();
        planetsNamesPageTwo = getPlanetsNamesPageTwo();
        return planetsNamesPageTwo;
    }




}
