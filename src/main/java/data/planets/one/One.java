package data.planets.one;
import java.util.ArrayList;

public class One {

    // Variables
    private static ArrayList residents = new ArrayList();
    private static ArrayList films = new ArrayList();


    // Values
    private static ArrayList getResidents() {
        residents.add("https://swapi.dev/api/people/1/");
        residents.add("https://swapi.dev/api/people/2/");
        residents.add("https://swapi.dev/api/people/4/");
        residents.add("https://swapi.dev/api/people/6/");
        residents.add("https://swapi.dev/api/people/7/");
        residents.add("https://swapi.dev/api/people/8/");
        residents.add("https://swapi.dev/api/people/9/");
        residents.add("https://swapi.dev/api/people/11/");
        residents.add("https://swapi.dev/api/people/43/");
        residents.add("https://swapi.dev/api/people/62/");
        return residents;
    }

    private static ArrayList getFilms() {
        films.add("https://swapi.dev/api/films/1/");
        films.add("https://swapi.dev/api/films/3/");
        films.add("https://swapi.dev/api/films/4/");
        films.add("https://swapi.dev/api/films/5/");
        films.add("https://swapi.dev/api/films/6/");
        return films;
    }

    // Getters

    public static ArrayList residents() {
        residents.clear();
        residents = getResidents();
        return residents;
    }

    public static ArrayList films() {
        films.clear();
        films = getFilms();
        return films;
    }


}
