package data.planets.two;
import java.util.ArrayList;

public class Two {

    // Variables
    private static ArrayList residents = new ArrayList();
    private static ArrayList films = new ArrayList();


    // Values
    private static ArrayList getResidents() {
        residents.add("https://swapi.dev/api/people/5/");
        residents.add("https://swapi.dev/api/people/68/");
        residents.add("https://swapi.dev/api/people/81/");
        return residents;
    }

    private static ArrayList getFilms() {
        films.add("https://swapi.dev/api/films/1/");
        films.add("https://swapi.dev/api/films/6/");
        return films;
    }

    // Getters

    public static ArrayList residents() {
        residents.clear();
        residents = getResidents();
        return residents;
    }

    public static ArrayList films() {
        films.clear();
        films = getFilms();
        return films;
    }

}
