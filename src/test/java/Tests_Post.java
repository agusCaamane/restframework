import io.restassured.http.ContentType;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;

public class Tests_Post {
    String baseUrl = "https://reqres.in/api";

    @Test //(enabled = false)
    public void testPost(){
//        Map<String, Object> map = new HashMap<String, Object>();
//
//        map.put("name", "NameTest1");
//        map.put("job", "JobTest1");
//
//        System.out.println(map);

        JSONObject request = new JSONObject();

        request.put("name", "NameTest1");
        request.put("job", "JobTest1");

        System.out.println(request);
        System.out.println(request.toJSONString());

        given().
                header("Content_type", "application/json").
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                body(request.toJSONString()).
        when().
                post(baseUrl + "/users").
        then().
                statusCode(201).
                log().all();
    }

    @Test
    public void testPut(){
        JSONObject request = new JSONObject();

        request.put("name", "NameTest1");
        request.put("job", "JobTest1");

        System.out.println(request);
        System.out.println(request.toJSONString());

        given().
                header("Content_type", "application/json").
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                body(request.toJSONString()).
        when().
                put(baseUrl + "/users/2").
        then().
                statusCode(200).
                log().all();
    }

    @Test
    public void testPatch(){
        JSONObject request = new JSONObject();

        request.put("name", "NameTest1");
        request.put("job", "JobTest1");

        System.out.println(request);
        System.out.println(request.toJSONString());

        given().
                header("Content_type", "application/json").
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                body(request.toJSONString()).
        when().
                patch(baseUrl + "/users/2").
        then().
                statusCode(200).
                log().all();
    }

    @Test
    public void testDelete(){

        when().
                delete(baseUrl + "/users/2").
        then().
                statusCode(204).
                log().all();
    }

}
