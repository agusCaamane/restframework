package starWars.people;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import static utils.Constants.StarWarsUrl;

public class Luke {

    @Test //(enabled = false)
    public void getLuke(){
        given().
                get(StarWarsUrl + "/people/1/").
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                //body("name", hasItems("Luke Skywalker", "Lindsay")).
                body("name", equalTo("Luke Skywalker")).
                body("height", equalTo("172")).
                body("mass", equalTo("77")).
                body("hair_color", equalTo("blond")).
                body("skin_color", equalTo("fair")).
                body("eye_color", equalTo("blue")).
                body("birth_year", equalTo("19BBY")).
                body("gender", equalTo("male")).
                body("homeworld", equalTo("https://swapi.dev/api/planets/1/")).
                body("films[0]", equalTo("https://swapi.dev/api/films/1/")).
                //body("films", equalTo(data.people.Luke.films())).
                //body("films", equalTo(data.people.Luke)).
                body("species", empty()).
                //body("vehicles", equalTo(data.people.Luke.vehicles())).
                //body("starships", equalTo(data.people.Luke.starShips())).
                body("created", equalTo("2014-12-09T13:50:51.644000Z")).
                body("edited", equalTo("2014-12-20T21:17:56.891000Z")).
                body("url", equalTo("https://swapi.dev/api/people/1/")).
                log().all();
        System.out.println(
                "The time taken to fetch the response to "+ StarWarsUrl + "/people/1/ " +
                        get(StarWarsUrl + "/people/1/").timeIn(TimeUnit.MILLISECONDS) + " milliseconds");
    }

    @Test //(enabled = false)
    public void postLuke(){
        given().
                post(StarWarsUrl + "/people/1/").
        then().
                statusCode(405).
                header("Content-Type", "application/json").
                body("detail", equalTo("Method 'POST' not allowed.")).
                log().all();
        System.out.println(
                "The time taken to fetch the response to "+ StarWarsUrl + "/people/1/ " +
                        post(StarWarsUrl + "/people/1/").timeIn(TimeUnit.MILLISECONDS) + " milliseconds");
    }


}
