package starWars.planets;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import static utils.Constants.StarWarsUrl;

public class Planets {

    @Test //(enabled = false)
    public void getPlanetspageOne(){
        given().
                get(StarWarsUrl + "/planets/").
        then().
                statusCode(200).
                body("count", equalTo(60)).
                body("next", equalTo("https://swapi.dev/api/planets/?page=2")).
                body("previous", nullValue()).
                body("results[0].name", equalTo("Tatooine")).
                body("results.name", equalTo(data.planets.Planets.planetsNamesPageOne())).
                body("results[0].films", equalTo(data.planets.one.One.films())).
                body("results[1].films", equalTo(data.planets.two.Two.films())).
                log().all();
        System.out.println(
                "The time taken to fetch the response to "+ StarWarsUrl + "/planets/ " +
                        get(StarWarsUrl + "/planets/").timeIn(TimeUnit.MILLISECONDS) + " milliseconds");
    }

    @Test //(enabled = false)
    public void getPlanetspageTwo(){
        given().
                param("page", 2).
                get(StarWarsUrl + "/planets/").
        then().
                statusCode(200).
                body("count", equalTo(60)).
                body("next", equalTo("http://swapi.dev/api/planets/?page=3")).
                body("previous", equalTo("http://swapi.dev/api/planets/?page=1")).
                body("results[0].name", equalTo("Geonosis")).
                body("results[0].terrain", equalTo("rock, desert, mountain, barren")).
                body("results.name", equalTo(data.planets.Planets.planetsNamesPageTwo())).
                log().all();
        System.out.println(
                "The time taken to fetch the response to "+ StarWarsUrl + "/planets/ " +
                        get(StarWarsUrl + "/planets/").timeIn(TimeUnit.MILLISECONDS) + " milliseconds");
    }

    @Test //(enabled = false)
    public void postPlanetspageOne(){
        given().
                post(StarWarsUrl + "/planets/").
        then().
                statusCode(405).
                header("Content-Type", "application/json").
                body("detail", equalTo("Method 'POST' not allowed.")).
                log().all();
        System.out.println(
                "The time taken to fetch the response to "+ StarWarsUrl + "/planets/ " +
                        post(StarWarsUrl + "/planets/").timeIn(TimeUnit.MILLISECONDS) + " milliseconds");
    }


}
