package starWars.planets.one;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalTo;
import static utils.Constants.StarWarsUrl;

public class One {

    @Test //(enabled = false)
    public void getPlanetsOne(){
        given().
                get(StarWarsUrl + "/planets/1/").
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("name", equalTo("Tatooine")).
                body("rotation_period", equalTo("23")).
                body("orbital_period", equalTo("304")).
                body("diameter", equalTo("10465")).
                body("climate", equalTo("arid")).
                body("gravity", equalTo("1 standard")).
                body("terrain", equalTo("desert")).
                body("surface_water", equalTo("1")).
                body("population", equalTo("200000")).
                body("residents[0]", equalTo("https://swapi.dev/api/people/1/")).
                body("residents", equalTo(data.planets.one.One.residents())).
                body("films", equalTo(data.planets.one.One.films())).
                body("created", equalTo("2014-12-09T13:50:49.641000Z")).
                body("edited", equalTo("2014-12-20T20:58:18.411000Z")).
                body("url", equalTo("https://swapi.dev/api/planets/1/")).
                log().all();
        System.out.println(
                "The time taken to fetch the response to "+ StarWarsUrl + "/planets/1/ " +
                        get(StarWarsUrl + "/planets/1/").timeIn(TimeUnit.MILLISECONDS) + " milliseconds");
    }

    @Test //(enabled = false)
    public void postPlanetsOne(){
        given().
                //queryParam("email", "test@gmail.com").
                //queryParam("pass", "pass123!").
        when().
        post(StarWarsUrl + "/planets/1/").
        then().
                statusCode(405).
                header("Content-Type", "application/json").
                body("detail", equalTo("Method 'POST' not allowed.")).
                log().all();
        System.out.println(
                "The time taken to fetch the response to "+ StarWarsUrl + "/planets/1/ " +
                        post(StarWarsUrl + "/planets/1/").timeIn(TimeUnit.MILLISECONDS) + " milliseconds");
    }
}
