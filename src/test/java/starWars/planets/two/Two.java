package starWars.planets.two;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalTo;
import static utils.Constants.StarWarsUrl;

public class Two {


    @Test //(enabled = false)
    public void getPlanetsTwo(){
        given().
                get(StarWarsUrl + "/planets/2/").
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("name", equalTo("Alderaan")).
                body("rotation_period", equalTo("24")).
                body("orbital_period", equalTo("364")).
                body("diameter", equalTo("12500")).
                body("climate", equalTo("temperate")).
                body("gravity", equalTo("1 standard")).
                body("terrain", equalTo("grasslands, mountains")).
                body("surface_water", equalTo("40")).
                body("population", equalTo("2000000000")).
                body("residents[0]", equalTo("http://swapi.dev/api/people/5/")).
                body("residents", equalTo(data.planets.two.Two.residents())).
                body("films", equalTo(data.planets.two.Two.films())).
                body("created", equalTo("2014-12-10T11:35:48.479000Z")).
                body("edited", equalTo("2014-12-20T20:58:18.420000Z")).
                body("url", equalTo("http://swapi.dev/api/planets/2/")).
                log().all();
        System.out.println(
                "The time taken to fetch the response to "+ StarWarsUrl + "/planets/2/ " +
                        get(StarWarsUrl + "/planets/2/").timeIn(TimeUnit.MILLISECONDS) + " milliseconds");
    }

    @Test //(enabled = false)
    public void postPlanetsTwo(){
        given().
                post(StarWarsUrl + "/planets/2/").
        then().
                statusCode(405).
                header("Content-Type", "application/json").
                body("detail", equalTo("Method 'POST' not allowed.")).
                log().all();
        System.out.println(
                "The time taken to fetch the response to "+ StarWarsUrl + "/planets/2/ " +
                        post(StarWarsUrl + "/planets/2/").timeIn(TimeUnit.MILLISECONDS) + " milliseconds");
    }


}
